package main

import (
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/emicklei/go-restful"
	"github.com/emicklei/go-restful/swagger"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// This example is functionally the same as the example in restful-user-resource.go
// with the only difference that is served using the restful.DefaultContainer

var (
	gMyDb *MyDb
)

type MyDb struct {
	session    *mgo.Session
	database   *mgo.Database
	collection *mgo.Collection
}

func NewMyDb() *MyDb {
	myDb := new(MyDb)

	var err error
	myDb.session, err = mgo.Dial("localhost")
	if err != nil {
		panic(err)
	}

	// Optional. Switch the session to a monotonic behavior.
	myDb.session.SetMode(mgo.Monotonic, true)
	myDb.database = myDb.session.DB("test")
	myDb.collection = myDb.database.C("users")

	return myDb
}

func (myDb *MyDb) destroy() {
	myDb.session.Close()
}

type User struct {
	Name, Pet string
}

type UserService struct {
	myDb *MyDb
}

func (u UserService) Register() {

	ws := new(restful.WebService)
	ws.
		Path("/users").
		Consumes(restful.MIME_JSON, restful.MIME_JSON).
		Produces(restful.MIME_JSON, restful.MIME_JSON) // you can specify this per route as well

	ws.Route(ws.GET("/").To(u.findAllUsers).
		// docs
		Doc("get all users").
		Operation("findAllUsers").
		Returns(200, "OK", []User{}))

	ws.Route(ws.GET("/{user-id}").To(u.findUser).
		// docs
		Doc("get a user").
		Operation("findUser").
		Param(ws.PathParameter("user-id", "identifier of the user").DataType("string")).
		Writes(User{})) // on the response

	ws.Route(ws.PUT("/{user-id}").To(u.updateUser).
		// docs
		Doc("update a user").
		Operation("updateUser").
		Param(ws.PathParameter("user-id", "identifier of the user").DataType("string")).
		Reads(User{})) // from the request

	ws.Route(ws.POST("").To(u.createUser).
		// docs
		Doc("create a user").
		Operation("createUser").
		Reads(User{})) // from the request

	ws.Route(ws.DELETE("/{user-id}").To(u.removeUser).
		// docs
		Doc("delete a user").
		Operation("removeUser").
		Param(ws.PathParameter("user-id", "identifier of the user").DataType("string")))

	restful.Add(ws)
}

// GET http://localhost:8080/users
//
func (u UserService) findAllUsers(request *restful.Request, response *restful.Response) {
	pageOffset, err := strconv.Atoi(request.QueryParameter("page[offset]"))
	if err != nil {
		pageOffset = 0
	}

	pageLimit, err := strconv.Atoi(request.QueryParameter("page[limit]"))
	if err != nil {
		pageLimit = 10
	}

	pageTotal, err := gMyDb.collection.Find(bson.M{}).Count();
	if err != nil {
		response.WriteErrorString(http.StatusNotFound, "Empty data")
		return
	}

	pageOffsetPrev := pageOffset-1
	if pageOffsetPrev<0{
		pageOffsetPrev=0
	}

	pageOffsetNext := pageOffset+1
	if pageOffsetNext>=pageTotal{
	  pageOffsetNext=pageTotal-1
	}

	usr := &[]User{}
	if err := gMyDb.collection.Find(bson.M{}).Skip(pageOffset).Limit(pageLimit).Sort("Name").All(usr); err != nil {
		response.WriteErrorString(http.StatusNotFound, "Empty data")
		return
	}
	// response.WriteEntity(usr)

	fmt.Println("  Method:", request.Request.Method, "  URL:", request.Request.URL, "  HOST:", request.Request.Host, "  RequestURI:", request.Request.RequestURI, "  Address:", request.Request.RemoteAddr)
	
	response.WriteEntity(bson.M{
		"links": bson.M{
			"self": fmt.Sprintf("http://%s/users?page[offset]=%d&page[limit]=%d", request.Request.Host, pageOffset, pageLimit), 
			"prev": fmt.Sprintf("http://%s/users?page[offset]=%d&page[limit]=%d", request.Request.Host, pageOffsetPrev, pageLimit), 
			"next": fmt.Sprintf("http://%s/users?page[offset]=%d&page[limit]=%d", request.Request.Host, pageOffsetNext, pageLimit) },

		"meta": bson.M{ "page": bson.M{ "offset": pageOffset, "limit": pageLimit, "total": pageTotal  } },
		"data": usr })

}

// GET http://localhost:8080/users/1
//
func (u *UserService) findUser(request *restful.Request, response *restful.Response) {

	id := request.PathParameter("user-id")
	if len(id) == 0 {
		response.WriteErrorString(http.StatusBadRequest, "Invalid Request")
		return
	}

	usr := &User{}
	if err := gMyDb.collection.FindId(bson.ObjectIdHex(id)).One(usr); err != nil {
		response.WriteErrorString(http.StatusNotFound, "User could not be found.")
		return
	}
	response.WriteEntity(usr)
}

// PUT http://localhost:8080/users/1
// <User><Id>1</Id><Name>Melissa Raspberry</Name></User>
//
func (u *UserService) updateUser(request *restful.Request, response *restful.Response) {

	id := request.PathParameter("user-id")
	usr := &User{}

	if err := request.ReadEntity(usr); err != nil {
		response.WriteError(http.StatusInternalServerError, err)
		return
	}

	fmt.Println(id)

	if err := gMyDb.collection.UpdateId(bson.ObjectIdHex(id), usr); err != nil {
		response.WriteError(http.StatusInternalServerError, err)
		return
	}

	response.WriteEntity(usr)

}

// PUT http://localhost:8080/users/1
// <User><Id>1</Id><Name>Melissa</Name></User>
//
func (u *UserService) createUser(request *restful.Request, response *restful.Response) {
	usr := &User{}
	if err := request.ReadEntity(usr); err != nil {
		response.WriteError(http.StatusInternalServerError, err)
		return
	}
	if err := gMyDb.collection.Insert(usr); err != nil {
		response.WriteError(http.StatusInternalServerError, err)
		return
	}
	response.WriteEntity(usr)
}

// DELETE http://localhost:8080/users/1
//
func (u *UserService) removeUser(request *restful.Request, response *restful.Response) {
	id := request.PathParameter("user-id")

	if len(id) == 0 {
		response.WriteErrorString(http.StatusBadRequest, "Invalid Request")
		return
	}

	if err := gMyDb.collection.RemoveId(bson.ObjectIdHex(id)); err != nil {
		response.WriteErrorString(http.StatusNotFound, "Cannot delete, user could not be found.")
		return
	}
	response.WriteEntity(bson.M{"_id": id})
}

func main() {

	u := UserService{}
	u.Register()

	gMyDb = NewMyDb()
	defer gMyDb.destroy()

	// Optionally, you can install the Swagger Service which provides a nice Web UI on your REST API
	// You need to download the Swagger HTML5 assets and change the FilePath location in the config below.
	// Open http://localhost:8080/apidocs and enter http://localhost:8080/apidocs.json in the api input field.
	config := swagger.Config{
		WebServices:    restful.RegisteredWebServices(), // you control what services are visible
		WebServicesUrl: "http://localhost:8080",
		ApiPath:        "/apidocs.json",

		// Optionally, specifiy where the UI is located
		SwaggerPath:     "/apidocs/",
		SwaggerFilePath: "./swagger-ui/dist"}
	swagger.InstallSwaggerService(config)

	log.Printf("start listening on localhost:8080")
	log.Fatal(http.ListenAndServe(":8080", nil))
}
